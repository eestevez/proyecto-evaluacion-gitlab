(function () {
    
    function funcionalidadA(a: number, b: number): number {
        return (a + b) / 2;
    }

    function funcionalidadB(a: number, b: number, c: number): number {
        return (a + b) * c;
    }

    function funcionalidadC(a: number, b: number): number {
        return a * b;
    }

    function funcionalidadD(a: number, b: number): number {
        return a / b;
    }

    function funcionalidadE(a: number): number {
        return Math.pow(a, 3);
    }

    function funcionalidadF(a: number): number {
        return Math.pow(a, 2);
    }

    function main() {
        funcionalidadA(2, 8);
        funcionalidadB(4, 7, 2);
        funcionalidadC(5, 2);
        funcionalidadD(48, 8);
        funcionalidadE(4);
        funcionalidadF(3);
    }

    main();
})();