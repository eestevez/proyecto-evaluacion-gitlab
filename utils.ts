export function caracteristicaExperimental(a: number, b: number, c: number, x: number): number {
    return a * Math.pow(x, 2) + b * x + c;
}